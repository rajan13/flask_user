#!/usr/bin/python
import sys
import logging
from config import Config
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/flask_user/")

from app import create_app
application = create_app(Config)
application.secret_key = 'This will be generated in a better way'