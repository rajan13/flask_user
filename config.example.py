import os, random
basedir = os.path.abspath(os.path.dirname(__file__))

secret_key = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in xrange(32))

class Config:
    # Flask settings
    SECRET_KEY = os.getenv('SECRET_KEY', secret_key)
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'DATABASE_URL',     'sqlite:///basic_app.sqlite')
    # SQLALCHEMY_BINDS = { 'test' : 'sqlite:///test.db' }
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MYSQL_DATABASE_HOST = 'localhost'
    MYSQL_DATABASE_USER = ''
    MYSQL_DATABASE_PASSWORD = ''
    MYSQL_DATABASE_DB = ''

    # Flask-Mail settings
    # MAIL_USERNAME =           os.getenv('MAIL_USERNAME',        'example@gmail.com')
    # MAIL_PASSWORD =           os.getenv('MAIL_PASSWORD',        'password')
    # MAIL_DEFAULT_SENDER =     os.getenv('MAIL_DEFAULT_SENDER',  '"MyApp" <example@gmail.com>')
    # MAIL_SERVER =             os.getenv('MAIL_SERVER',          'smtp.gmail.com')
    # MAIL_PORT =           int(os.getenv('MAIL_PORT',            '587'))
    # MAIL_SUBJECT_PREFIX =     '[Flask Mailer]'
    # MAIL_USE_SSL =        int(os.getenv('MAIL_USE_SSL',         True))
