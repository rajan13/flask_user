import os
from flask import Flask, url_for
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
from flaskext.mysql import MySQL





mysql = MySQL()
db = SQLAlchemy()
mail = Mail()
bootstrap = Bootstrap()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)


    db.init_app(app)
    # db.Model.metadata.reflect(db.engine)
    mysql.init_app(app)

    mail.init_app(app)
    login_manager.init_app(app)
    bootstrap.init_app(app)
    # with app.app_context():
    #     db.Model.metadata.reflect(db.engine)


    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix='/register/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/register/admin/auth')

    return app
