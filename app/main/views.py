from . import main
from ..models import db
from .. import mysql
from flask import render_template
from flask_login import login_required , current_user

@main.route('/')
def index():
    return render_template('index.html')


@main.route('/students')
@login_required
def students():
    return render_template('dashboard.html')


@main.route('/students/<id>')
@login_required
def student(id):
    pass

@main.route('/uhd/<id>')
def uhd(id):
    cur = mysql.get_db().cursor()
    cur.execute('''SELECT * FROM USER WHERE ID = %s''' % id)
    rv = cur.fetchall()
    return str(rv[0][2])
